﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using ProyectoWebServices.Models;

namespace ProyectoWebServices.Controllers
{
    public class ReunionesController : ApiController
    {
        private DBModelReunion db = new DBModelReunion();

        // GET: api/Reuniones
        public IQueryable<Reunion> GetReunions()
        {
            return db.Reunions;
        }

        // GET: api/Reuniones/5
        [ResponseType(typeof(Reunion))]
        public IHttpActionResult GetReunion(int id)
        {
            Reunion reunion = db.Reunions.Find(id);
            if (reunion == null)
            {
                return NotFound();
            }

            return Ok(reunion);
        }

        // PUT: api/Reuniones/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutReunion(int id, Reunion reunion)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != reunion.Id)
            {
                return BadRequest();
            }

            db.Entry(reunion).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ReunionExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Reuniones
        [ResponseType(typeof(Reunion))]
        public IHttpActionResult PostReunion(Reunion reunion)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Reunions.Add(reunion);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = reunion.Id }, reunion);
        }

        // DELETE: api/Reuniones/5
        [ResponseType(typeof(Reunion))]
        public IHttpActionResult DeleteReunion(int id)
        {
            Reunion reunion = db.Reunions.Find(id);
            if (reunion == null)
            {
                return NotFound();
            }

            db.Reunions.Remove(reunion);
            db.SaveChanges();

            return Ok(reunion);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ReunionExists(int id)
        {
            return db.Reunions.Count(e => e.Id == id) > 0;
        }
    }
}
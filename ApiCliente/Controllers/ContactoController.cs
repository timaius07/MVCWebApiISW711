﻿using ApiCliente.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace ApiCliente.Controllers
{
    public class ContactoController : Controller
    {
        // GET: Contacto
        public ActionResult Index()
        {
			IEnumerable<mvcContactoModel> contList;
			HttpResponseMessage response = GlobalVariables.WebApiContact.GetAsync("Contactos").Result;
			contList =  response.Content.ReadAsAsync<IEnumerable<mvcContactoModel>>().Result;
			return View(contList);
		}
		public ActionResult ListarClientes()
		{
			
			IEnumerable<mvcClienteModel> contList;
			HttpResponseMessage response = GlobalVariables.WebApiClient.GetAsync("Clientes").Result;
			contList = response.Content.ReadAsAsync<IEnumerable<mvcClienteModel>>().Result;
			return PartialView(contList);
		}
		public ActionResult AgregarOEditar(int id = 0)
		{
			if (id == 0)
				return View(new mvcContactoModel());
			else
			{
				HttpResponseMessage response = GlobalVariables.WebApiContact.GetAsync("Contactos/" + id.ToString()).Result;
				return View(response.Content.ReadAsAsync<mvcContactoModel>().Result);
			}
		}
		[HttpPost]
		public ActionResult AgregarOEditar(mvcContactoModel contc)
		{
			if (contc.Id == 0)
			{
				HttpResponseMessage response = GlobalVariables.WebApiContact.PostAsJsonAsync("Contactos", contc).Result;
				TempData["SuccessMessage"] = "Exito al Guardar";
			}
			else
			{
				HttpResponseMessage response = GlobalVariables.WebApiContact.PutAsJsonAsync("Contactos/" + contc.Id, contc).Result;
				TempData["SuccessMessage"] = "Exito al Modificar";
			}

			return RedirectToAction("Index");

		}
		public ActionResult Eliminar(int id)
		{
			HttpResponseMessage response = GlobalVariables.WebApiContact.DeleteAsync("Contactos/" + id.ToString()).Result;
			TempData["SuccessMessage"] = "Eliminado Exitosamente";
			return RedirectToAction("Index");
		}

		
	}
	}
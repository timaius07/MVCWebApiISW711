﻿using ApiCliente.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace ApiCliente.Controllers
{
    public class UsuarioController : Controller
    {
        // GET: Usuario
        public ActionResult Index()
        {
			IEnumerable<mvcUsuarioModel> usuList;
			HttpResponseMessage response = GlobalVariables.WebApiUsuario.GetAsync("Usuarios").Result;
			usuList = response.Content.ReadAsAsync<IEnumerable<mvcUsuarioModel>>().Result;
			return View(usuList);
		}

		public ActionResult AgregarOEditar(int id = 0)
		{
			if (id == 0)
				return View(new mvcUsuarioModel());
			else
			{
				HttpResponseMessage response = GlobalVariables.WebApiUsuario.GetAsync("Usuarios/" + id.ToString()).Result;
				return View(response.Content.ReadAsAsync<mvcUsuarioModel>().Result);
			}
		}
		[HttpPost]
		public ActionResult AgregarOEditar(mvcUsuarioModel usu)
		{
			if (usu.Id == 0)
			{
				HttpResponseMessage response = GlobalVariables.WebApiUsuario.PostAsJsonAsync("Usuarios", usu).Result;
				TempData["SuccessMessage"] = "Exito al Guardar";
			}
			else
			{
				HttpResponseMessage response = GlobalVariables.WebApiUsuario.PutAsJsonAsync("Usuarios/"+usu.Id, usu).Result;
				TempData["SuccessMessage"] = "Exito al Modificar";
			}
			return RedirectToAction("Index");

		}
		public ActionResult Eliminar(int id)
		{
			HttpResponseMessage response = GlobalVariables.WebApiUsuario.DeleteAsync("Usuarios/" + id.ToString()).Result;
			TempData["SuccessMessage"] = "Eliminado Exitosamente";
			return RedirectToAction("Index");
		}
	}
}
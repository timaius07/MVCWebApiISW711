﻿using ApiCliente.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace ApiCliente.Controllers
{
    public class TicketController : Controller
    {
        // GET: Ticket
        public ActionResult Index()
        {
			IEnumerable<mvcTicketModel> contList;
			HttpResponseMessage response = GlobalVariables.WebApiTicket.GetAsync("Tickets").Result;
			contList = response.Content.ReadAsAsync<IEnumerable<mvcTicketModel>>().Result;
			return View(contList);
		}
		public ActionResult ListarClientes()
		{
			IEnumerable<mvcClienteModel> contList;
			HttpResponseMessage response = GlobalVariables.WebApiClient.GetAsync("Clientes").Result;
			contList = response.Content.ReadAsAsync<IEnumerable<mvcClienteModel>>().Result;
			return PartialView(contList);
		}
		public ActionResult AgregarOEditar(int id = 0)
		{
			if (id == 0)
				return View(new mvcTicketModel());
			else
			{
				HttpResponseMessage response = GlobalVariables.WebApiTicket.GetAsync("Tickets/" + id.ToString()).Result;
				return View(response.Content.ReadAsAsync<mvcTicketModel>().Result);
			}
		}
		[HttpPost]
		public ActionResult AgregarOEditar(mvcTicketModel contc)
		{
			if (contc.Id == 0)
			{
				HttpResponseMessage response = GlobalVariables.WebApiTicket.PostAsJsonAsync("Tickets", contc).Result;
				TempData["SuccessMessage"] = "Exito al Guardar";
			}
			else
			{
				HttpResponseMessage response = GlobalVariables.WebApiTicket.PutAsJsonAsync("Tickets/" + contc.Id, contc).Result;
				TempData["SuccessMessage"] = "Exito al Modificar";
			}

			return RedirectToAction("Index");

		}
		public ActionResult Eliminar(int id)
		{
			HttpResponseMessage response = GlobalVariables.WebApiTicket.DeleteAsync("Tickets/" + id.ToString()).Result;
			TempData["SuccessMessage"] = "Eliminado Exitosamente";
			return RedirectToAction("Index");
		}
	}
}
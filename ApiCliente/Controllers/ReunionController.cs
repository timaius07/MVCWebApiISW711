﻿using ApiCliente.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace ApiCliente.Controllers
{
    public class ReunionController : Controller
    {
        // GET: Reunion
        public ActionResult Index()
        {
			IEnumerable<mvcReunionModel> reunList;
			HttpResponseMessage response = GlobalVariables.WebApiReunion.GetAsync("Reuniones").Result;
			reunList = response.Content.ReadAsAsync<IEnumerable<mvcReunionModel>>().Result;
			return View(reunList);
		}

		public ActionResult ListarUsuarios()
		{
			IEnumerable<mvcUsuarioModel> contList;
			HttpResponseMessage response = GlobalVariables.WebApiUsuario.GetAsync("Usuarios").Result;
			contList = response.Content.ReadAsAsync<IEnumerable<mvcUsuarioModel>>().Result;
			return PartialView(contList);
		}

		public ActionResult AgregarOEditar(int id = 0)
		{
			if (id == 0)
				return View(new mvcReunionModel());
			else
			{
				HttpResponseMessage response = GlobalVariables.WebApiReunion.GetAsync("Reuniones/" + id.ToString()).Result;
				return View(response.Content.ReadAsAsync<mvcReunionModel>().Result);
			}
		}

		[HttpPost]
		public ActionResult AgregarOEditar(mvcReunionModel usu)
		{
			if (usu.Id == 0)
			{
				HttpResponseMessage response = GlobalVariables.WebApiReunion.PostAsJsonAsync("Reuniones", usu).Result;
				TempData["SuccessMessage"] = "Exito al Guardar";
			}
			else
			{
				HttpResponseMessage response = GlobalVariables.WebApiReunion.PutAsJsonAsync("Reuniones/" + usu.Id, usu).Result;
				TempData["SuccessMessage"] = "Exito al Modificar";
			}
			return RedirectToAction("Index");

		}
		public ActionResult Eliminar(int id)
		{
			HttpResponseMessage response = GlobalVariables.WebApiReunion.DeleteAsync("Reuniones/" + id.ToString()).Result;
			TempData["SuccessMessage"] = "Eliminado Exitosamente";
			return RedirectToAction("Index");
		}
	}
}
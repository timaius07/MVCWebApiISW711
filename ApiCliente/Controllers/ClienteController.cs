﻿using ApiCliente.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace ApiCliente.Controllers
{
    public class ClienteController : Controller
    {
        // GET: Cliente
        public ActionResult Index()
        {
			IEnumerable<mvcClienteModel> cliList;
			HttpResponseMessage response = GlobalVariables.WebApiClient.GetAsync("Clientes").Result;
			cliList = response.Content.ReadAsAsync<IEnumerable<mvcClienteModel>>().Result;
			return View(cliList);
        }

		public ActionResult AgregarOEditar(int id=0)
		{
			if (id == 0)
				return View(new mvcClienteModel());
			else {
				HttpResponseMessage response = GlobalVariables.WebApiClient.GetAsync("Clientes/"+id.ToString()).Result;
				return View(response.Content.ReadAsAsync<mvcClienteModel>().Result);
			}
			
		}
		[HttpPost]
		public ActionResult AgregarOEditar(mvcClienteModel cli)
		{
			if (cli.id==0)
			{
				HttpResponseMessage response = GlobalVariables.WebApiClient.PostAsJsonAsync("Clientes", cli).Result;
				TempData["SuccessMessage"] = "Exito al Guardar";
			}
			else
			{
				HttpResponseMessage response = GlobalVariables.WebApiClient.PutAsJsonAsync("Clientes/"+cli.id , cli).Result;
				TempData["SuccessMessage"] = "Exito al Modificar";
			}
			
			return RedirectToAction("Index");
			
		}
		public ActionResult Eliminar(int id)
		{
			HttpResponseMessage response = GlobalVariables.WebApiClient.DeleteAsync("Clientes/" + id.ToString()).Result;
			TempData["SuccessMessage"] = "Eliminado Exitosamente";
			return RedirectToAction("Index");
		}
	}
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ApiCliente.Models
{
	public class mvcContactoModel
	{
		public int Id { get; set; }
		[Required(ErrorMessage = " Este campo es requerido")]
		[Display(Name = "Nombre Cliente")]
		public int Id_Cliente { get; set; }
		[Required(ErrorMessage = " Este campo es requerido")]
		[Display(Name = "Nombre Contacto")]
		public string Nombre { get; set; }
		[Required(ErrorMessage = " Este campo es requerido")]
		[Display(Name = "Apellidos")]
		public string Apellidos { get; set; }
		[Required(ErrorMessage = " Este campo es requerido")]
		[Display(Name = "E-mail")]
		public string Correo_Electronico { get; set; }
		[Required(ErrorMessage = " Este campo es requerido")]
		[Display(Name = "Teléfono")]
		public string Telefono { get; set; }
		[Required(ErrorMessage = " Este campo es requerido")]
		[Display(Name = "Puesto")]
		public string Puesto { get; set; }
		public virtual ApiCliente.Models.mvcClienteModel Cliente { get; set; }
	}
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ApiCliente.Models
{
	public class mvcClienteModel
	{
		public int id { get; set; }
		[Required(ErrorMessage = " Este campo es requerido")]
		[Display(Name = "Nombre")]
		public string Nombre { get; set; }
		[Required(ErrorMessage = " Este campo es requerido")]
		[Display(Name = "Ced-Júridica")]
		public string Ced_Juridica { get; set; }
		[Required(ErrorMessage = " Este campo es requerido")]
		[Display(Name = "Página Web")]
		public string Pag_Web { get; set; }
		[Required(ErrorMessage = " Este campo es requerido")]
		[Display(Name = "# Télefono")]
		public string Telefono { get; set; }
		[Required(ErrorMessage = " Este campo es requerido")]
		[Display(Name = "Sector")]
		public string Sector { get; set; }
	}
}
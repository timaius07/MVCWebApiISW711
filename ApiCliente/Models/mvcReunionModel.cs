﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ApiCliente.Models
{
	public class mvcReunionModel
	{
		public int Id { get; set; }
		[Required(ErrorMessage = " Este campo es requerido")]
		[Display(Name = "Título Reunión")]
		public string Titulo_Reunion { get; set; }
		[Required(ErrorMessage = " Este campo es requerido")]
		[Display(Name = "Fecha")]
		[DataType(DataType.Date)]
		public System.DateTime Fecha_Hora { get; set; }
		[Required(ErrorMessage = " Este campo es requerido")]
		[Display(Name = "Usuario Asignado")]
		public int Id_Usuario { get; set; }
		[Required(ErrorMessage = " Este campo es requerido")]
		[Display(Name = "Reunión Virtual")]
		public bool Virtual { get; set; }
		public virtual ApiCliente.Models.mvcUsuarioModel  Usuario { get; set; }
	}
}
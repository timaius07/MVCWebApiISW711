﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ApiCliente.Models
{
	public class mvcUsuarioModel
	{
		public int Id { get; set; }
		[DisplayName("Nombre")]
		[Required(ErrorMessage = "Debe digitar un nombre de usuario")]
		public string Nombre_Usuario { get; set; }
		[DisplayName("Contraseña")]
		[DataType(DataType.Password)]
		[Required(ErrorMessage = "Debe digitar una contraseña")]
		public string Contrasena { get; set; }
		[DisplayName("Rol")]
		public string Rol_Usuario { get; set; }
		[DisplayName("Apellido")]
		[Required(ErrorMessage = "Debe digitar un apellido al usuario")]
		public string Apellido_Usuario { get; set; }
	}
}
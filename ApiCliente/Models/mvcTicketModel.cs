﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApiCliente.Models
{
	public class mvcTicketModel
	{
		public int Id { get; set; }
		public string Titulo_Problema { get; set; }
		public string Detalle { get; set; }
		public string Reporta { get; set; }
		public int Cliente { get; set; }
		public string EstadoActual { get; set; }
		public virtual ApiCliente.Models.mvcClienteModel Cliente1 { get; set; }
	}
}
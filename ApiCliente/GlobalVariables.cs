﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;

namespace ApiCliente
{
	public static class GlobalVariables
	{
		public static HttpClient WebApiClient = new HttpClient();
		public static HttpClient WebApiContact = new HttpClient();
		public static HttpClient WebApiUsuario = new HttpClient();
		public static HttpClient WebApiReunion = new HttpClient();
		public static HttpClient WebApiTicket = new HttpClient();

		static GlobalVariables()
		{
			WebApiClient.BaseAddress = new Uri("http://localhost:52088/api/");
			WebApiClient.DefaultRequestHeaders.Clear();
			WebApiClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

			WebApiContact.BaseAddress = new Uri("http://localhost:52088/api/");
			WebApiContact.DefaultRequestHeaders.Clear();
			WebApiContact.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

			WebApiUsuario.BaseAddress = new Uri("http://localhost:52088/api/");
			WebApiUsuario.DefaultRequestHeaders.Clear();
			WebApiUsuario.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

			WebApiReunion.BaseAddress = new Uri("http://localhost:52088/api/");
			WebApiReunion.DefaultRequestHeaders.Clear();
			WebApiReunion.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

			WebApiTicket.BaseAddress = new Uri("http://localhost:52088/api/");
			WebApiTicket.DefaultRequestHeaders.Clear();
			WebApiTicket.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

		}
	}
}